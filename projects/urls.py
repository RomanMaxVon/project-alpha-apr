from django.urls import path
from . import views

urlpatterns = [
    path("", views.list_projects, name="list_projects"),
    path("<int:id>/", views.view_task_info, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
