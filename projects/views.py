from django.shortcuts import render
from .models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from django.shortcuts import redirect


# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"project_list": projects}
    return render(request, "projects/projects_listed.html", context)


@login_required
def view_task_info(request, id):
    project = Project.objects.get(id=id)
    return render(request, "projects/details.html", {"project": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
