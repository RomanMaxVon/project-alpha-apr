from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView
from accounts.views import signup_page

# from .models import

urlpatterns = [
    path("login/", views.login_page, name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", signup_page, name="signup"),
]
