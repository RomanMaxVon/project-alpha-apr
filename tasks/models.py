from django.db import models
from projects.models import Project
from django.conf import settings

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="tasks"
    )
    assignee = models.ForeignKey(
        USER_MODEL, on_delete=models.CASCADE, null=True, related_name="tasks"
    )
