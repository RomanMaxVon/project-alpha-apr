from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from django.shortcuts import redirect
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            task = form.save()
            task.assignee = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


def list_tasks(request):
    task_model_listed = Task.objects.filter(assignee=request.user)
    context = {"task_model_listed": task_model_listed}
    return render(request, "tasks/show_tasks.html", context)
